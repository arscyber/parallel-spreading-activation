# README #

This algorithm uses parallel Breadth-first search in order to build layers and a path through these layers.
The path is usually the greatest weight path.

The layers are based on semantic closeness. The closer the layers, the closer they are semantically.
The path represents the semantically closest path.

This algorithm can be modified for various real-time shortest path searches, modeling trajectories, even more sophisticated data mining processes, and more.
The trajectories found can often be interesting and revealing. Accumulations of information around some point are also often interesting and revealing.

# Text generation adaptation #
If you want to use this for text generation, add a parse tree filter to the algorithm core - only choose tree branches that correspond to the parse tree.
Enable randomness if you want more random searches.

# Morphemes #
Break the text onto morpheme chunks for more interesting information.

# Layers #
Perform Fourrier analysis and break it up onto layers for even more information, down to the neurolinguistic level and neurolinguistic mismatch information.

# Parallel Spreading Activation Layers #
![Parallel Spreading Activation Screenshot](https://bitbucket.org/arscyber/parallel-spreading-activation/raw/1fe5c709608c33639dfd36262d034e3b8be4591e/screenshot_1.png)

# Parallel Spreading Activation Oscillation Activity #
![Parallel Spreading Activation Screenshot](https://bitbucket.org/arscyber/parallel-spreading-activation/raw/726f59c44d650c2a84c34dcf1f044b057ee4c528/activity_1.png)


# Comments: #
They say irreversible computation can increase entropy. After all, information generates energy. Landauer's principle.
And reversible logic gates that are used in quantum computation hold the highest reserve of such energy that is then reduced through calculation and extracted with irreversible logic gates. 
Degenerate sources of energy are by far not the only sources of energy. There are far more, and energy can be resisted, capacitated, inducted.

From the water comes the fish.